# School Tag Buddy #
_Android_

Participants playing _School Tag_ that use a phone instead of stickers or transit cards.

* Using location services, the student can receive credits for walking, biking or taking the bus and arriving
to school on time.. 
* As a _big buddy_, students and adults with this app can help a _little buddy_ get to school and earn points.
* Parents who drive can earn points for all participants of a carpool.

The rules above are configured by the actual [school-tag-cloud](../school-tag-cloud] installation, but 
gives an idea what the mobile application can do beyond the normal participant.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact